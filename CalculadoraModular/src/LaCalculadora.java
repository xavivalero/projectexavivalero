import java.util.Scanner;

public class LaCalculadora {
	
	static Scanner reader = new Scanner(System.in);
	static int num1 = 0;
	static int num2 = 0;
	static int resultat = 0;
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
	
		IntrodueixPrimerNumero();
		
		PresentaMenu();
		
		RespostaMenu();
		
		
	}
	 public static void PresentaMenu() {
		System.out.println("Men d'opcions");
		System.out.println("1.Sumar");
		System.out.println("2.Restar");
		System.out.println("3.Multiplicar");
		System.out.println("4.Dividir");
		System.out.println("5.Visualitzar el resultat");
		System.out.println("6.Sortir del programa");
	}
	public static void RespostaMenu() {
		
		int opcio = 0;
		
		boolean seguim = true;
		
		do {
			if(seguim) {
				IntrodueixSegonNumero();
			}
			
			System.out.println("Introdueix la opci que vols");
			boolean correcte = reader.hasNextInt();
			
			
			if(correcte) {
				opcio = reader.nextInt();
				if(opcio < 1 || opcio > 6) {
					System.out.println("Aquesta opci no est disponible");
				} 
			} else {
				System.out.println("El tipus de dades introduides s erroni");
			}
			
		switch(opcio) {
		case 1:
		
		Sumar(num1,num2);
		
		break;
		case 2:
			
		Restar(num1, num2);
		
		break;
		case 3:
			
		Multiplicar(num1, num2);
			
		break;
		case 4:
			
		Dividir(num1, num2);
			
		break;
		case 5:
			
		Resultat(num1);
		
		break;
		case 6:
			System.out.println("Programa tancat");
			seguim = false;
		break;
		}
		
		}while(seguim);
		
		
	}
	public static int IntrodueixPrimerNumero() {
		
		boolean integral = false;
		
		System.out.println("Introdueix el primer nmero que vols operar");
		boolean correcte = reader.hasNextInt();
		
		do {
			if(correcte) {
				num1 = reader.nextInt();
				integral = true;
			}else {
				reader.nextLine();
				System.out.println("El tipus de dada introduida no s vlid");
				System.out.println("Torna a introduir el nmero");
			}
		}while(!integral);
		
		return num1;
	}
	public static int IntrodueixSegonNumero() {
		
		boolean integral = false;
		
		System.out.println("Introdueix el nmero que vols operar");
		boolean correcte = reader.hasNextInt();
		
		do {
			if(correcte) {
				num2 = reader.nextInt();
				integral = true;
			}else {
				reader.nextLine();
				System.out.println("El tipus de dada introduida no s vlid");
				System.out.println("Torna a introduir el nmero");
			}
		}while(!integral);
		
		return num2;
	}
	public static int Sumar( int numero1, int numero2) {
		
		num1 = numero1 + numero2;
		
		return num1;
		
	}
	public static int Restar(int numero1, int numero2) {
		
		num1 = numero1 - numero2;
		
		return num1;
	}
	public static int Multiplicar(int numero1, int numero2) {
		
		num1 = numero1 * numero2;                      
		
		return num1;
	}
	public static void Dividir(int numero1, int numero2) {
		
		int opcio = 0;
		
		System.out.println("Qu vols obtenir de la divisi?");
		System.out.println("1.El residu");
		System.out.println("2.El quocient");
		opcio = reader.nextInt();
		
		if(opcio == 1) {
			CalcularResidu(num1,num2);
		} else if(opcio == 2) {
			CalcularQuocient(num1, num2);
		}else {
			System.out.println("No existeix aquesta opci");
		}
		
		
	}
	public static int CalcularResidu(int numero1, int numero2) {
		
		num1 = numero1 % numero2;
		
		return num1;
	}
	public static int CalcularQuocient(int numero1, int numero2) {
		
		num1 = numero1 / numero2;
		
		return num1;
	}
	public static void Resultat(int resultado) {
		
		System.out.println("El resultat s " + resultado);
	}
	
}
